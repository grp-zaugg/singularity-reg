# singularity-reg

This repository collects Singularity/Apptainer images/containers from the Zaugg group and will be populated over time.
It is maintained by Max and Christian.

Currently, it has the followng images:

- `alpine` - my base container that has R/Bioconductor and devtools.
- `rocker_r-ver_py` - Less minimal base container based on [rocker](https://rocker-project.org/) docker images that has python3, R/Bioconductor, devtools and tidyverse installed.
- `rocker_tidyverse_py` - Even less minimal base container based on [rocker](https://rocker-project.org/) docker images (with full Rstudio server installed) that has python3, R/Bioconductor, devtools and tidyverse installed.
- `atac_conda` - container for the ATAC-Seq pipeline (all command line tools)
- `atac_r` - container for the ATAC-Seq pipeline (R + all required packages)
- `rna_conda` - container for the RNA-Seq pipeline (all command line tools)
- `rna_fastqc` - container for the RNA-Seq pipeline (fastqc)
- `rna_r` - container for the RNA-Seq pipeline (R + all required packages)
- `scenic` - container for the SCENIC pipeline. Also based on alpine.

You can use any of `alpine`, `rocker_r-ver_py` or `rocker_tidyverse_py` images
as a base for your own containers, but their size gets progressively larger.
The upside of `rocker` is that they are based on Ubuntu, so installing some system
packages might be easier and there might generally be less friction.
Or just use your own base image.

Both the ATAC and RNA images have `stable` tags and are meant to be used for the
stable (master) version of the pipelines. In the future, `dev` may be added for development versions of the pipeline.

The snakemake pipelines in the lab reference the publicly accessible URL for
these images, while the images can also be downloaded manually:
`apptainer pull oras://registry.git.embl.de/grp-zaugg/singularity-reg/atac_conda:stable`

> **Python**: PEP 668 introduced externally managed environments, which on many
linux distributions stops you from modifying system python installation, including
installing packages with `pip`. The solution is to create virtual environment
and make sure that it is activated whenever container is run.

